# Communication visuelle


## Format
Sur place (ou en ligne). En français.
Présentation de notions théoriques, présentation de cas. Exercices dirigés en groupes.

## Objectifs
- pouvoir préparer des éléments visuels à destination des médias électroniques
- construire des éléments visuels concis et engageants à partir d'une matière complexe
- comprendre les bases des codes et des registres de l'expression graphique
- connaître des moyens pour réaliser des compositions graphiques simples 

## Public
Doctorant-es et post-doctorant-es

## Descriptif supplémentaire
L'atelier de communication visuelle tend à fournir les connaissances théoriques et 
les moyens techniques pour réaliser des éléments graphiques dans le but de communiquer 
des messages à destination d'un public cible. À travers une alternance de présentation
de notions théoriques et d'ateliers pratiques les participant-es seront amené-es à
se poser les bonnes questions et à mobiliser les meilleurs moyens pour atteindre 
leur objectif communicationnel.

## L'animateur
Julien Jespersen travaille à la Division de la formation et des étudiant-es (DIFE) 
en tant que développeur web. Il a travaillé en tant que graphiste durant de nombreuses 
années aux Activités culturelles ainsi qu’au Service de communication de l’UNIGE. 
De plus il anime des cours de mise en page et d’illustration au sein de la DIFE.

## Ressources
- logiciel de graphisme, p.ex. [Affinity Designer](https://affinity.serif.com/fr/designer/)
- polices de caractères, p.ex [Google Fonts](https://fonts.google.com/)
- banque d'images, p.ex. [Unsplash](https://unsplash.com/)
- icônes vectorielles, p.ex [Fontawesome](https://fontawesome.com/icons?d=gallery)

## Références
- Facebook formats d'image: (https://www.dreamgrow.com/facebook-cheat-sheet-sizes-and-dimensions/) obsolète!
- [Facebook help](https://www.facebook.com/help/266520536764594?helpref=uf_permalink&rdrhc)
- Instagram formats d'image: (https://www.tailwindapp.com/blog/instagram-image-size-guide-2020)
- Youtube formats d'image: (https://support.google.com/youtube/answer/2972003)
- Cours de graphsime à L'Université de Genève: (https://unige.ch/dife/culture/cours/image)

## Plan
### Bases du graphisme
#### dimensions, orientation
Avoir le "spectateur" à l’esprit; se mettre dans la peau du destinataire. Expérience de proximité, rapport "intime". Expérience grand format, expérience "publique".

![Rapport audience](img/rapport_audience.png)

#### pleins, vides (negative space)
équilibre des masses

![masses](img/masses.png)

marges, espacements

![marges](img/marges.png)

utlisation des vides: "aires de repos", mise en valeur des éléments, le vide est le signe

![vide solo](img/vide_solo.png)
![vide noise](img/vide_noise.png)
![vide negative space](img/vide_negative_space.png)

#### positionement
composer selon une **grille** donne de la consistance

![grille](img/grille.png)

les **marges** permettent de distinguer les éléments, aussi pour des raisons techniques

**aligner** les objets c'est leur donner une raison d'être, éviter le flottement 

![aligne](img/aligne.png)

#### couleurs
une couleur peut signifier un chose ou une autre selon la culture, selon la personne ou encore le contexte

![color sens](img/color_sens.png)

il est souvent plus efficace de limiter le nombre de couleurs

![color how much](img/color_howmuch.png)
![color max](img/color_max.png)

certaines couleurs sont moins chargées de sens que d'autres

![color forte](img/color_forte.png)

comme nous vivons sur la "Planète bleue" nous sommes habitué à cette couleur 

![color sure](img/color_sure.png)

dans chaque culture les associations de couleurs suggèrent un registre

![color registres](img/color_registres.png)

#### typographie
texte à voir

![texte family](img/txt_family.png)

texte à lire

![texte statut](img/txt_statut.png)

l'aspect annonce le statut du texte

![texte registre](img/txt_registre.png)

la forme _est_ le message (registre), corélation entre signifiant et signifié (sème)

![color registres](img/color_registres.png)

combien de polices de caractères différentes?

![texte max](img/txt_2max.png)

polices de caractères mal encodées, polices de car. de mauvais "goût"

![texte bad](img/txt_badtypo.png)
![texte dont](img/txt_dont.png)

#### Cadrage / photographie
![cadres plans](https://upload.wikimedia.org/wikipedia/commons/2/2e/Cadre_plan.png)

dynamisme de la composition, du cadrage

![photo portraits](img/photo_portraits.png)
![photo cadrage](img/photo_cadrage.png)

effets et détourage

![photo effet](img/photo_effet.png)

combiner les registres: rasters / vectoriels

![photo picto](img/photo_picto.png)



### Composition graphique
#### consistance
Les couleurs sont toujours les mêmes à travers les déclinaisons et les itérations (le temps). La (les) polices de caractères sont les mêmes. Le traitement photographique est le même. Tout cela crée votre _image_, votre identité.
#### efficacité
Vous ne pouvez pas tout raconter à chaque fois à tout le monde. Sélectionnez minutieusement les items et les termes (les mots) pour chaque visuel. "Tu m'as captivé, et ensuite quoi?"
#### registre
Votre composition, votre esthétique va camper l'ambiance, va définir le registre, que vous le vouliez ou non. Mieux vaut maîtriser ceci. 
#### cohérence du message
Le registre des polices de car., le choix des couleurs, le choix des termes (mots), devraient tendre vers le même univers culturel, le même registre social ou professionnel.
### Destinataire du message
#### qui
Qui est votre interloctueur cible? Quel est son référent culturel? Quelle langue parle-t-il? Quel est son métier? Quel âge a-t-il? …
### quoi, action
Qu'attendez-vous de lui? Que doit-il faire s'il vous est acquis? Que doit-il retenir de vous? Que doit-il retenir de votre projet?
### Temps limité
- avec le temps à disposition (2 sec.?) il est plus efficace de capitaliser sur ce qui a déjà été fait. Utiliser les codes en place pour communiquer
- notion d'espace

### Comment communiquer?
- avoir un objectif (réaliste)
- s'adresser à un interloctueur (réel ou virtuel)
- présenter un enjeu, un challenge
- maîtriser les aspects émotionels ou créer des émotions

### Contraintes techniques (monde numérique)
#### dimension d'image (format)
- largeur
- hauteur
- résolution (?)
- unité de mesure: pixel (px)
#### mode de l'image
- raster, mode point, bitmap
- vectoriel, mode objet, (script)
- poids de fichier
#### type de fichier
- .png .jpg (.jpeg) .webp .svg
#### compression
- destructive (i.e. .jpg)
- préservative (i.e .png)

### Aides techniques
- https://www.photopea.com
- https://vectr.com
- https://krita.org/en/

